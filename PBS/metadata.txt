﻿# See the documentation on the wiki to learn how to edit this file.
#-------------------------------
[000]
PlayerA = POKEMONTRAINER_Red,trchar000,boy_bike,boy_surf,boy_run,boy_surf,boy_fish_offset,boy_fish_offset
PlayerB = POKEMONTRAINER_Leaf,trchar001,girl_bike,girl_surf,girl_run,girl_surf,girl_fish_offset,girl_fish_offset
TrainerVictoryME = Battle victory trainer.ogg
WildVictoryME = Battle victory wild.ogg
TrainerBattleBGM = Battle trainer.mid
SurfBGM = Surfing.mid
BicycleBGM = Bicycle.mid
WildCaptureME = Battle capture success.ogg
Home = 43,12,2,2
WildBattleBGM = Battle wild.mid
#-------------------------------
[001]
# Intro
#-------------------------------
[002]
# Molten Steel Pit
BattleBack = cave
MapPosition = 0,13,12
Outdoor = false
Environment = Volcano
ShowArea = true
#-------------------------------
[003]
# Break Room
MapPosition = 0,13,12
HealingSpot = 2,8,8
#-------------------------------
[004]
# Umbal City Com. District
BattleBack = city
Bicycle = true
TrainerVictoryME = Battle victory trainer.ogg
TrainerBattleBGM = Battle trainer.mid
MapPosition = 0,13,12
Outdoor = true
Environment = None
ShowArea = true
#-------------------------------
[005]
# Even Badderlands
BattleBack = field
MapPosition = 0,13,11
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[006]
# Transition Cave
BattleBack = Cave
Bicycle = true
MapPosition = 0,13,11
Outdoor = false
ShowArea = true
#-------------------------------
[007]
# Mall
MapSize = 2,11
MapPosition = 0,13,10
Outdoor = false
ShowArea = true
#-------------------------------
[008]
# Mall
MapPosition = 0,13,12
#-------------------------------
[009]
# Mall
MapPosition = 0,14,10
HealingSpot = 8,17,11
#-------------------------------
[010]
# Elevator
BattleBack = indoor3
MapPosition = 0,13,10
Environment = Rock
#-------------------------------
[011]
# Project Building
MapPosition = 0,14,10
#-------------------------------
[012]
# Umbal City Gym
BattleBack = indoor1
Bicycle = false
TrainerVictoryME = Battle victory leader.ogg
MapPosition = 0,14,10
Outdoor = false
BicycleAlways = false
Environment = None
WildBattleBGM = Battle Gym Leader.mid
ShowArea = false
#-------------------------------
[013]
# Frijed City
BattleBack = Snow
Weather = Snow,50
Bicycle = true
TrainerVictoryME = Battle victory trainer.ogg
Dungeon = false
TrainerBattleBGM = Battle trainer.mid
MapPosition = 0,14,10
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[014]
# Opulance City
BattleBack = city
Bicycle = true
TrainerVictoryME = Battle victory trainer.ogg
WildVictoryME = Battle victory wild.ogg
TrainerBattleBGM = Battle trainer.mid
MapPosition = 0,13,10
Outdoor = true
Environment = None
WildCaptureME = Battle capture success.ogg
WildBattleBGM = Battle wild.mid
ShowArea = true
#-------------------------------
[015]
# Breezee Town
BattleBack = Field
Weather = Rain,10
Bicycle = true
TrainerVictoryME = Battle victory leader.ogg
TrainerBattleBGM = Battle Gym Leader.mid
MapPosition = 0,13,10
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[016]
# Pokemon Daycare
MapPosition = 0,13,10
#-------------------------------
[017]
# Poké Center
MapPosition = 0,13,10
#-------------------------------
[018]
# PokeMart
MapPosition = 0,13,10
#-------------------------------
[019]
# Frijed Inn
MapPosition = 0,13,10
#-------------------------------
[020]
# Frijed Fields
BattleBack = Snow
Weather = Snow,50
Bicycle = true
TrainerVictoryME = Battle victory trainer.ogg
WildVictoryME = Battle victory wild.ogg
TrainerBattleBGM = Battle trainer.mid
MapPosition = 0,13,10
Outdoor = true
Environment = Grass
WildCaptureME = Battle capture success.ogg
WildBattleBGM = Battle wild.mid
ShowArea = true
#-------------------------------
[021]
# Starter Park
MapSize = 1,11
BattleBack = field
Bicycle = false
WildVictoryME = Battle victory wild.ogg
MapPosition = 0,14,8
Outdoor = true
Environment = Grass
WildCaptureME = Battle capture success.ogg
WildBattleBGM = Battle wild.mid
ShowArea = true
#-------------------------------
[022]
# Shelter Cave
BattleBack = Cave
Bicycle = false
WildVictoryME = Battle victory wild.ogg
Outdoor = false
Environment = Cave
WildCaptureME = Battle capture success.ogg
WildBattleBGM = Battle wild.mid
ShowArea = true
#-------------------------------
[023]
# Opulance Inn
MapPosition = 0,15,8
Outdoor = true
ShowArea = true
#-------------------------------
[024]
# Poké Center
MapPosition = 0,15,8
HealingSpot = 23,11,15
#-------------------------------
[025]
# Record Shop
MapPosition = 0,15,8
#-------------------------------
[026]
# Rundown Building
BattleBack = IndoorC
Bicycle = false
TrainerVictoryME = Battle victory trainer.ogg
WildVictoryME = Battle victory wild.ogg
TrainerBattleBGM = Battle trainer.mid
MapPosition = 0,15,8
Outdoor = false
BicycleAlways = false
Environment = None
WildCaptureME = Battle capture success.ogg
WildBattleBGM = Battle wild.mid
ShowArea = true
#-------------------------------
[027]
# Rundown Building
BattleBack = IndoorC
Bicycle = false
TrainerVictoryME = Battle victory trainer.ogg
WildVictoryME = Battle victory wild.ogg
TrainerBattleBGM = Battle trainer.mid
MapPosition = 0,15,8
Outdoor = true
BicycleAlways = false
WildCaptureME = Battle capture success.ogg
WildBattleBGM = Battle wild.mid
ShowArea = false
#-------------------------------
[028]
# Hidden Cave
BattleBack = field
MapPosition = 0,16,8
Outdoor = false
ShowArea = true
#-------------------------------
[029]
# Hidden Cave
Bicycle = false
MapPosition = 0,16,8
Outdoor = false
ShowArea = false
#-------------------------------
[030]
# Hidden Cave
MapPosition = 0,16,8
Outdoor = false
ShowArea = false
#-------------------------------
[031]
# Hidden Hideaway
MapSize = 2,1101
BattleBack = field
MapPosition = 0,14,6
Outdoor = true
ShowArea = true
#-------------------------------
[032]
# Umbal City
BattleBack = city
Bicycle = true
Outdoor = true
BicycleAlways = false
Environment = None
HealingSpot = 32,18,9
ShowArea = true
#-------------------------------
[033]
# New Day Plain
BattleBack = Field
Bicycle = true
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[034]
# Opulance Market
BattleBack = cave1
Bicycle = true
MapPosition = 0,15,6
Environment = Cave
#-------------------------------
[035]
# Poke Ball Factory
MapPosition = 0,13,6
Outdoor = true
ShowArea = true
#-------------------------------
[036]
# Opulance City Mall
MapPosition = 0,13,6
HealingSpot = 35,17,7
#-------------------------------
[037]
# Opulance City Mall
BattleBack = elite1
MapPosition = 0,13,6
#-------------------------------
[038]
# Opulance City Mall
MapPosition = 0,13,6
#-------------------------------
[039]
# Poké Center
MapSize = 2,11
BattleBack = field
MapPosition = 0,11,6
Outdoor = true
ShowArea = true
#-------------------------------
[040]
# PokeMart
MapPosition = 0,11,6
Outdoor = false
BicycleAlways = false
ShowArea = false
#-------------------------------
[041]
# Windmill Way
MapSize = 1,111
BattleBack = Field
Weather = Rain,10
Bicycle = true
TrainerVictoryME = Battle victory trainer.ogg
WildVictoryME = Battle victory wild.ogg
TrainerBattleBGM = Battle trainer.mid
MapPosition = 0,11,7
Outdoor = true
BicycleAlways = false
WildCaptureME = Battle capture success.ogg
WildBattleBGM = Battle wild.mid
ShowArea = true
#-------------------------------
[042]
# Winding Woods
BattleBack = Field
Bicycle = true
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[044]
# Breezee Dam
MapSize = 2,11
BattleBack = field
Weather = Rain,10
MapPosition = 0,11,10
Outdoor = true
ShowArea = true
#-------------------------------
[045]
# ???
BattleBack = IndoorC
TrainerVictoryME = Battle victory leader.ogg
TrainerBattleBGM = Battle Elite.mid
MapPosition = 0,11,10
Outdoor = false
BicycleAlways = false
Environment = None
ShowArea = true
#-------------------------------
[046]
# Trainer's School
Bicycle = true
MapPosition = 0,12,6
#-------------------------------
[047]
# Breezee Inn
MapSize = 2,11
BattleBack = rocky
Weather = Rain,0
MapPosition = 0,15,10
Outdoor = true
Environment = Rock
ShowArea = true
#-------------------------------
[048]
# The Cavern of Knowledge
BattleBack = Cave
Bicycle = true
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[049]
# Snowbound Sanctuary
BattleBack = cave1
Bicycle = true
MapPosition = 0,16,10
Environment = Cave
#-------------------------------
[050]
# Opulance Suburbs
BattleBack = cave3
Bicycle = true
MapPosition = 0,16,10
DarkMap = true
Environment = Cave
#-------------------------------
[051]
# Windswept Farms
BattleBack = cave2
Bicycle = true
Dungeon = true
MapPosition = 0,16,10
Environment = Cave
#-------------------------------
[052]
MapPosition = 0,17,10
Outdoor = true
ShowArea = true
#-------------------------------
[053]
MapPosition = 0,17,10
HealingSpot = 52,17,14
#-------------------------------
[054]
MapPosition = 0,17,10
#-------------------------------
[055]
MapPosition = 0,17,10
HealingSpot = 52,30,10
#-------------------------------
[056]
BattleBack = indoor1
MapPosition = 0,17,10
#-------------------------------
[057]
MapPosition = 0,17,10
#-------------------------------
[058]
MapPosition = 0,17,10
#-------------------------------
[059]
BattleBack = indoor1
MapPosition = 0,17,10
#-------------------------------
[060]
MapPosition = 0,17,10
#-------------------------------
[061]
BattleBack = indoor1
MapPosition = 0,17,10
#-------------------------------
[062]
MapPosition = 0,17,10
#-------------------------------
[063]
MapPosition = 0,17,10
#-------------------------------
[064]
BattleBack = indoor1
MapPosition = 0,17,10
#-------------------------------
[065]
MapPosition = 0,17,10
#-------------------------------
[066]
BattleBack = field
MapPosition = 0,12,12
Outdoor = true
ShowArea = true
#-------------------------------
[067]
MapPosition = 0,12,12
#-------------------------------
[068]
BattleBack = forest
MapPosition = 0,12,12
SafariMap = true
Outdoor = true
Environment = Forest
ShowArea = true
#-------------------------------
[069]
DiveMap = 70
BattleBack = field
MapPosition = 0,13,13
Outdoor = true
ShowArea = true
#-------------------------------
[070]
BattleBack = underwater
MapPosition = 0,13,13
Environment = Underwater
#-------------------------------
[071]
MapPosition = 0,13,13
#-------------------------------
[072]
BattleBack = field
Weather = Storm,50
MapPosition = 0,18,17
Outdoor = true
ShowArea = true
#-------------------------------
[073]
BattleBack = field
MapPosition = 0,22,16
Outdoor = true
ShowArea = true
#-------------------------------
[074]
Bicycle = true
MapPosition = 0,12,10
#-------------------------------
[075]
BattleBack = city
MapPosition = 1,13,16
Outdoor = true
#-------------------------------
[076]
# Lillith's Home
Bicycle = true
Outdoor = true
ShowArea = false
#-------------------------------
[077]
# Lillith's Home
#-------------------------------
[081]
# Craggy Pass
BattleBack = field
Bicycle = true
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[082]
# Cast Iron Cavern
BattleBack = Cave
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[083]
# Badlands
BattleBack = field
Bicycle = true
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[085]
# Umbal City Gym
BattleBack = indoor1
Bicycle = false
TrainerVictoryME = Battle victory trainer.ogg
TrainerBattleBGM = Battle trainer.mid
Outdoor = false
Environment = None
ShowArea = false
#-------------------------------
[086]
# Cold Steel Cavern
BattleBack = cave
Bicycle = true
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[088]
# Miser Marsh
BattleBack = Field
Weather = Rain,25
Bicycle = true
Outdoor = true
Environment = Puddle
ShowArea = true
#-------------------------------
[089]
# Dragon's Den
BattleBack = Cave
Bicycle = true
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[090]
# Mythical Field
BattleBack = Forest
Bicycle = true
Outdoor = true
Environment = Grass
ShowArea = true
#-------------------------------
[091]
# Icebound Pit
BattleBack = cave
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[092]
# Rider's Hideout
BattleBack = cave
Bicycle = true
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[093]
# Rider's Hideout
BattleBack = cave
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[094]
# Rider's Hideout
BattleBack = cave
Bicycle = true
Outdoor = false
Environment = Cave
ShowArea = true
#-------------------------------
[095]
# Rider's Hideout
BattleBack = cave
Bicycle = true
Outdoor = false
Environment = Cave
ShowArea = true
