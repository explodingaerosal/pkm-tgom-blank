﻿# See the documentation on the wiki to learn how to edit this file.
#-------------------------------
# Frijed Fields (20) - Frijed City (13)
20,East,0,13,West,0
# Winding Woods (42) - New Day Plain (33)
42,South,0,33,North,0
# Lillith's Home (76) - New Day Plain (33)
76,East,0,33,West,0
# Mythical Field (90) - Miser Marsh (88)
90,South,0,88,North,0
# Umbal City Com. District (4) - Miser Marsh (88)
4,East,39,88,West,0
# Starter Park (21) - Miser Marsh (88)
21,East,65,88,West,0
# Umbal City (32) - Miser Marsh (88)
32,East,0,88,West,11
# Craggy Pass (81) - Badlands (83)
81,West,0,83,East,7
# Umbal City Com. District (4) - Mythical Field (90)
4,East,0,90,West,11
# Starter Park (21) - Mythical Field (90)
21,East,15,90,West,0
# Umbal City (32) - Mythical Field (90)
32,East,0,90,West,61
# Lillith's Home (76) - Winding Woods (42)
76,North,30,42,South,0
# Umbal City (32) - Winding Woods (42)
32,South,0,42,North,19
# Umbal City Com. District (4) - Craggy Pass (81)
4,West,43,81,East,0
# Umbal City (32) - Craggy Pass (81)
32,West,0,81,East,7
# Starter Park (21) - Umbal City Com. District (4)
21,South,0,4,North,14
# Umbal City (32) - Umbal City Com. District (4)
32,North,0,4,South,0
# Windmill Way (41) - Breezee Dam (44)
41,East,0,44,West,0
# Breezee Town (15) - Windmill Way (41)
15,North,0,41,South,10
